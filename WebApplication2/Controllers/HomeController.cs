﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    [CustomActionFilter]
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetNotes()
        {
            return Json(NoteWork.GetNoteNames(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetContent(string name)
        {
            return Content(NoteWork.GetContent(name));
        }

        [HttpGet]
        public ActionResult Image(string name)
        {
            Image img = ImageHelper.ConvertToImage(name);
            Stream stream = ImageHelper.ConvertToStream(img);

            return File(stream, "image/png");
        }

        [HttpGet]
        public ActionResult PageCreate()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Show(string name)
        {
            ViewBag.Name = name;
            return View("Index");
        }

        [HttpPost]
        public ActionResult Create(CreatingNote note)
        {
            if (!NoteWork.CreateNote(note.Name))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult Save(string name, string content)
        {
            NoteWork.SaveContent(name, content);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}