﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace WebApplication2.Models
{
    public class SqlServerFactory
    {
        private string connectionString;

        public SqlServerFactory(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Log(string controller, string action, string browser)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "INSERT INTO NOTE_LOG (CONTROLLER, ACTION, BROWSER, DATE) VALUES(@C, @A, @B, @D);";
                    command.Parameters.AddWithValue("@C", controller);
                    command.Parameters.AddWithValue("@A", action);
                    command.Parameters.AddWithValue("@B", browser);
                    command.Parameters.AddWithValue("@D", DateTime.Now);
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}