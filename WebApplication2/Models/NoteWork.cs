﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class NoteWork
    {
        private static string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data");

        public static bool CreateNote(string name)
        {
            string file = Path.Combine(path, name);

            if (File.Exists(file))
            {
                return false;
            }

            File.AppendAllText(file, "");

            return true;
        }

        public static string[] GetNoteNames()
        {
            return Directory.GetFiles(path).Select(file => Path.GetFileName(file)).ToArray();
        }

        public static string GetContent(string name)
        {
            return File.ReadAllText(Path.Combine(path, name));
        }

        public static void SaveContent(string name, string content)
        {
            string file = Path.Combine(path, name);

            if (!File.Exists(file))
            {
                return;
            }

            File.WriteAllText(file, content);
        }
    }
}