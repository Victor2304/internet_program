﻿using System.Web.Mvc;

namespace WebApplication2.Models
{
    public class CustomActionFilter : ActionFilterAttribute
    {
        static SqlServerFactory factory = new SqlServerFactory("Data Source=.;Initial Catalog=MY_NOTE;Integrated Security=SSPI;");

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controller = filterContext.RouteData.GetRequiredString("Controller");
            string action = filterContext.RouteData.GetRequiredString("Action");
            string browser = filterContext.HttpContext.Request.Browser.Browser;

            factory.Log(controller, action, browser);
        }
    }
}