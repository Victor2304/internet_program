﻿using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Models
{
    public class CustomBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;
            string name = request.Form.Get("name");
            return new CreatingNote
            {
                Name = name
            };
        }
    }

    public class CreatingNote
    {
        public string Name { get; set; }
    }
}