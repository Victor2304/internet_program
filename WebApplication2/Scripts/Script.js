﻿$(document).ready(function() {
    ko.applyBindings(new ViewModel());
});

function Model() {
    this.createNote = function(noteName) {
        return $.post("/Home/Create", { name: noteName });
    };

    this.getNotes = function() {
        return $.get("/Home/GetNotes");
    };

    this.getContent = function(name) {
        return $.get("/Home/GetContent/" + name);
    };

    this.saveContent = function(name, content) {
        return $.post("/Home/Save", { name: name, content: content });
    };
}

function ViewModel() {
    this.model = new Model();
    this.noteName = ko.observable();
    this.notes = ko.observableArray();
    this.noteContent = ko.observable();
    this.image = ko.observable();
    var self = this;
    var currentNote;

    this.updateNotes = function() {
        this.model.getNotes().success(function (data) {
            self.notes(data);
        });
    }

    this.updateNotes();

    this.btnCreateClick = function() {
        this.model.createNote(this.noteName()).success(function() {
            self.noteName("");
            self.updateNotes();
        }).error(function() {
            alert("Блокнот уже существует");
        });
    };

    this.open = function(note) {
        currentNote = note;
        self.model.getContent(note).success(function(data) {
            self.noteContent(data);
        });
        self.image("/image/" + note);
    };

    this.saveContent = function() {
        this.model.saveContent(currentNote, this.noteContent());
    };
}