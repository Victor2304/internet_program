﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication2
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Empty",
                url: "",
                defaults: new
                {
                    controller = "Home",
                    action = "Index"
                }
            );

            routes.MapRoute(
                name: "Create",
                url: "note/create",
                defaults: new
                {
                    controller = "Home",
                    action = "PageCreate",
                    name = UrlParameter.Optional
                }
            );

            routes.MapRoute(
                name: "Note",
                url: "note/{name}",
                defaults: new
                {
                    controller = "Home",
                    action = "Show",
                    name = UrlParameter.Optional
                }
            );

            routes.MapRoute(
                name: "Image",
                url: "image/{name}",
                defaults: new { controller = "Home", action = "Image", name = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{name}",
                defaults: new { name = UrlParameter.Optional }
            );
        }
    }
}
